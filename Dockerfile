FROM golang:1.15.2

MAINTAINER Ahmad Habibi ashabibi49@gmail.com
ADD . /go/src/latmvc2

ARG appname=e-document-api
ARG http_port=1323

RUN go get -d github.com/gin-contrib/cors
RUN go get -d github.com/gin-gonic/gin
RUN go get -d golang.org/x/crypto/bcrypt
RUN go get -d github.com/dgrijalva/jwt-go
RUN go get -d gorm.io/gorm
RUN go get -d github.com/mitchellh/mapstructure
RUN go install latmvc2

ENTRYPOINT /go/bin/latmvc2

ENV PORT $http_port
ENV DB_HOST 192.168.8.102
ENV DB_PORT 3306
ENV DB_USER root
ENV DB_PASS password

EXPOSE $http_port

RUN mkdir -p /go/src/github.com/latmvc2
COPY . /go/src/github.com/latmvc2
WORKDIR /go/src/github.com/latmvc2

CMD go run main.go